from setuptools import setup

setup(
    name="snake",
    version="1.0",
    install_requires=['tensorflow-gpu==1.14.0, tensorlayer-gpu=1.10.1, pygame'],
    author='Stephan Dumasy',
)
